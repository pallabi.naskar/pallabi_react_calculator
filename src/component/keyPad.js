import React, {Component} from 'react';

class KeyPadComponent extends Component {

    render() {
        return (
            <div className="button">
                <button id="sign" name="(" onClick={e => this.props.onClick(e.target.name)}>(</button>
                <button id="sign" name="C" onClick={e => this.props.onClick(e.target.name)}>DEL</button>
                <button id="sign" name=")" onClick={e => this.props.onClick(e.target.name)}>)</button>
                <button id="sign" name="CE" onClick={e => this.props.onClick(e.target.name)}> ⌫ </button><br/>


                <button name="1" onClick={e => this.props.onClick(e.target.name)}>1</button>
                <button name="2" onClick={e => this.props.onClick(e.target.name)}>2</button>
                <button name="3" onClick={e => this.props.onClick(e.target.name)}>3</button>
                <button id="sign" name="+" onClick={e => this.props.onClick(e.target.name)}>+</button><br/>

                <button name="4" onClick={e => this.props.onClick(e.target.name)}>4</button>
                <button name="5" onClick={e => this.props.onClick(e.target.name)}>5</button>
                <button name="6" onClick={e => this.props.onClick(e.target.name)}>6</button>
                <button id="sign" name="-" onClick={e => this.props.onClick(e.target.name)}>-</button><br/>

                <button name="7" onClick={e => this.props.onClick(e.target.name)}>7</button>
                <button name="8" onClick={e => this.props.onClick(e.target.name)}>8</button>
                <button name="9" onClick={e => this.props.onClick(e.target.name)}>9</button>
                <button id="sign" name="*" onClick={e => this.props.onClick(e.target.name)}>x</button><br/>

                <button name="sin" onClick={e => this.props.onClick(e.target.name)}>sin</button>
                <button name="cos" onClick={e => this.props.onClick(e.target.name)}>cos</button>
                <button name="tan" onClick={e => this.props.onClick(e.target.name)}>tan</button>
                <button id="sign" name="/" onClick={e => this.props.onClick(e.target.name)}>÷</button><br/>


                <button name="log" onClick={e => this.props.onClick(e.target.name)}>log</button>
                <button name="sq" onClick={e => this.props.onClick(e.target.name)}>a ²</button>
                <button name="√" onClick={e => this.props.onClick(e.target.name)}>√</button>
                <button id="sign" name="." onClick={e => this.props.onClick(e.target.name)}>.</button><br/>



                <button name="π" onClick={e => this.props.onClick(e.target.name)}>π</button>
                <button name="τ" onClick={e => this.props.onClick(e.target.name)}>τ</button>
                <button name="0" onClick={e => this.props.onClick(e.target.name)}>0</button>
                <button id="sign" name="=" onClick={e => this.props.onClick(e.target.name)}>=</button><br/>
            </div>
        );
    }
}


export default KeyPadComponent;