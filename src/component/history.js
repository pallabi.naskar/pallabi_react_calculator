import React, {Component} from 'react';
import './../calculator_design.css';
class HistoryComponent extends Component {


    render() {
        let {result}= this.props;
        
        return (
            <div className="history_result">
                 <ul>
          {result.map(item => (
            <li key={item}>{item}</li>
          ))}
        </ul>
            </div>
    )
        ;
    }
}


export default HistoryComponent;