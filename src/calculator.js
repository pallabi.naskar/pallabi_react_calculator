import React, { Component } from "react";
import ResultComponent from "./component/result_display";
import KeyPadComponent from "./component/keyPad";
import HistoryComponent from "./component/history";
import "./calculator_design.css";

var evaluate = require('static-eval');
var parse = require('esprima').parse;

class calculator extends Component {
  constructor() {
    super();

    this.state = {
      result: "",
      expression: "",
      finalResult: "",
      arrayValue: [],
      tempArray: [],
    };
  }
  onClick = (button) => {
    if (button === "√" || this.state.tempArray[0] === "√") {
      this.setState((state) => {
        const tempArray = [...state.tempArray, button];
        const result = [...state.tempArray, button];
        return {
          tempArray,
          result,
        };
      });
      if (Number.isInteger(parseInt(button)) === false && button !== "√") {
        this.mathematicalFunction(button, "√");
      }
    } else if (button === "sin" || this.state.tempArray[0] === "sin") {
      this.setState((state) => {
        const tempArray = [...state.tempArray, button];
        const result = [...state.tempArray, button];
        return {
          tempArray,
          result,
        };
      });
      if (Number.isInteger(parseInt(button)) === false && button !== "sin") {
        this.mathematicalFunction(button, "sin");
      }
    } else if (button === "tan" || this.state.tempArray[0] === "tan") {
      this.setState((state) => {
        const tempArray = [...state.tempArray, button];
        const result = [...state.tempArray, button];
        return {
          tempArray,
          result,
        };
      });
      if (Number.isInteger(parseInt(button)) === false && button !== "tan") {
        this.mathematicalFunction(button, "tan");
      }
    } else if (button === "cos" || this.state.tempArray[0] === "cos") {
      this.setState((state) => {
        const tempArray = [...state.tempArray, button];
        const result = [...state.tempArray, button];
        return {
          tempArray,
          result,
        };
      });
      if (Number.isInteger(parseInt(button)) === false && button !== "cos") {
        this.mathematicalFunction(button, "cos");
      }
    } else if (button === "log" || this.state.tempArray[0] === "log") {
      this.setState((state) => {
        const tempArray = [...state.tempArray, button];
        const result = [...state.tempArray, button];
        return {
          tempArray,
          result,
        };
      });
      if (Number.isInteger(parseInt(button)) === false && button !== "log") {
        this.mathematicalFunction(button, "log");
      }
    } else if (button === "=") {
      this.calculate();
    } else if (button === "C") {
      this.reset();
    } else if (button === "CE") {
      this.backspace();
    } else if (button === "sq") {
      this.squareValue();
    } else {
      this.setState({
        result: this.state.result + button,
      });
    }
  };

  calculate = () => {
    this.setState({
      expression: this.state.result,
    });

    var checkResult = "";
    if (this.state.result.includes("--")) {
      checkResult = this.state.result.replace("--", "+");
    }
    if (this.state.result.includes("π")) {
      checkResult = this.state.result.replace("π", "*3.14");
    }
    if (this.state.result.includes("τ")) {
      checkResult = this.state.result.replace("τ", "*6.28");
    } else {
      checkResult = this.state.result;
    }

    try {
      this.setState({
        result: (evaluate(parse(checkResult).body[0].expression) || "") + "",
        finalResult: (evaluate(parse(checkResult).body[0].expression) || "") + "",
      });
    } catch (e) {
      this.setState({
        result: "error",
      });
    }
    console.log(`${checkResult}=${this.setState.result}`);

    this.setState((state) => {
      const arrayValue = [
        ...state.arrayValue,
        `${state.expression}=${state.result}`,
      ];

      return {
        arrayValue,
      };
    });
  };

  squareValue = () => {
    try {
      this.setState({
        result: Math.pow(evaluate(parse(this.state.result).body[0].expression) || "", 2),
      });
    } catch (e) {
      this.setState({
        result: "error",
      });
    }
  };

  mathematicalFunction = (val, functionValue) => {
    console.log("in root");
    let value = this.state.tempArray.splice(1, this.state.tempArray.length - 1);
    let value1 = value.reduce((a, b) => {
      return a + b;
    });
    console.log("splice value " + value1);
    const sliceValue = this.state.result.slice(0, -(value.length + 1));

    if (functionValue === "√") {
      try {
        this.setState({
          result: sliceValue + `${Math.sqrt(value1).toFixed(4)}` + val,
          tempArray: "",
        });
      } catch (e) {
        this.setState({
          result: "error",
        });
      }
    } else if (functionValue === "tan") {
      try {
        this.setState({
          result: sliceValue + `${Math.tan(value1).toFixed(4)}` + val,
          // arrayValue:this.state.result,
          tempArray: "",
        });
      } catch (e) {
        this.setState({
          result: "error",
        });
      }
    } else if (functionValue === "sin") {
      try {
        this.setState({
          result: sliceValue + `${Math.sin(value1).toFixed(4)}` + val,
          // arrayValue:sliceValue+ `${(Math.sin(value1)).toFixed(4)}`+val,
          tempArray: "",
        });
      } catch (e) {
        this.setState({
          result: "error",
        });
      }
    } else if (functionValue === "cos") {
      try {
        this.setState({
          result: sliceValue + `${Math.cos(value1).toFixed(4)}` + val,
          tempArray: "",
        });
      } catch (e) {
        this.setState({
          result: "error",
        });
      }
    } else if (functionValue === "log") {
      try {
        this.setState({
          result: sliceValue + `${Math.log10(value1).toFixed(4)}` + val,
          tempArray: "",
        });
      } catch (e) {
        this.setState({
          result: "error",
        });
      }
    }
    this.setState((state) => {
      const arrayValue = [...state.arrayValue, `${functionValue}${value1}=${state.result}`];

      return {
        arrayValue,
      };
    });
  };

  reset = () => {
    this.setState({
      result: "",
    });
  };

  backspace = () => {
    this.setState({
      result: this.state.result.slice(0, -1),
    });
  };

  render() {
    return (
      <div class="total_body">
        <div className="calculator-body">
          <h1> CALCULATOR</h1>
          {console.log(this.state.result)}
          <ResultComponent result={this.state.result} />
          <KeyPadComponent onClick={this.onClick} />
        </div>
        <div id="history">
          <h3>History</h3>
          <HistoryComponent id="a" result={this.state.arrayValue} />
        </div>
      </div>
    );
  }
}

export default calculator;
