import React from 'react';
import About from "./component/About";
import Calculator from './calculator';
import HistoryComponent from "./component/history";
import Menu from './Menu';
import { Route } from 'react-router-dom';

const App=()=>{
  return(
    <div>
      <Menu/>
      <switch>
      <Route exact path="/home" component={Calculator}/>
        <Route exact path="/calculator" component={Calculator}/>
        <Route  path="/about" component={About}/>
        <Route  path="/history" component={HistoryComponent}/>  
      </switch>
    </div>
  )
}

export default App;