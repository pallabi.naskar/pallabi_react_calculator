import React from 'react';
import {NavLink} from "react-router-dom";
const Menu=()=>{
    return(
        <div class="menu">
        <NavLink exact activeClassName="active_class" to="/home">Home</NavLink>
        <NavLink exact activeClassName="active_class" to="/about">About</NavLink>
        <NavLink exact activeClassName="active_class" to="/calculator">Calculator</NavLink>
        </div>
    )
}

export default Menu;